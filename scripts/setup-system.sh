#!/bin/sh

# Setup hostname
echo $1 > /etc/hostname

# Generate locales (only en_US.UTF-8 for now)
sed -i -e '/en_US\.UTF-8/s/^# //g' /etc/locale.gen
locale-gen

# Change plymouth default theme
plymouth-set-default-theme mobian

# Load phosh on startup if package is installed
#if [ -f /usr/bin/phosh ]; then
#    systemctl enable phosh.service
#fi

# LightDM Tweaks
sed -i '/^#greeter-hide-users/ s/^#//' /etc/lightdm/lightdm.conf
echo -e "indicators=~a11y;~session;~power\nkeyboard=onboard --layout=Compact --theme=Nightshade\nkeyboard-position = 50,center -0;100%,25%\na11y-states=+keyboard" >> /etc/lightdm/lightdm-gtk-greeter.conf
dconf write /org/gnome/desktop/a11y/applications/screen-keyboard-enabled true
sed -i '/^[Service]/a Environment="LANG=en_US.UTF-8"' /usr/lib/systemd/system/lightdm.service
sed -i '/^#exit-on-failure=false/a xserver-command = X -nocursor' /etc/lightdm/lightdm.conf
